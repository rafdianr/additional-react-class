import {
  combineReducers
} from "redux"
import counterRed from './counterRed'
import albums from './albums'
import user from './user'
import washme from './washme'

export default combineReducers({
  counterRed,
  albums,
  user,
  washme
})