import React from "react";

const Card = ({ data }) => {
  return (
    <div className="card">
      <div className="card__wrapper">
        {data.map((item) => (
          <div key={item.id} className="card__item">
            <div className="card__body">
              <div className="card__thumbnail">
                <img src={item.thumbnailUrl} alt="" />
              </div>
              <div className="card__content">
                <div className="card__title">{item.title}</div>
                <div className="card__description">{item.url}</div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Card;
