export const getUser = (payload) => {
  return {
    type: "GET_USER",
    payload
  }
}

export const logoutUser = () => {
  return {
    type: "LOGOUT_USER"
  }
}