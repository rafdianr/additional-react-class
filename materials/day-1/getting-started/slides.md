---
# try also 'default' to start simple
theme: default
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://source.unsplash.com/collection/94734566/1920x1080
# apply any windi css classes to the current slide
layout: "cover"
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# some information about the slides, markdown enabled
info: |
  ## Getting Started
  Presentation slides for react additional class.
---

# Getting Started

React JS basic

<div class="pt-12">
  <span @click="$slidev.nav.next" class="px-2 p-1 rounded cursor-pointer" hover="bg-white bg-opacity-10">
    Press Space for next page <carbon:arrow-right class="inline"/>
  </span>
</div>

<a href="https://github.com/slidevjs/slidev" target="_blank" alt="GitHub"
  class="abs-br m-6 text-xl icon-btn opacity-50 !border-none !hover:text-white">
<carbon-logo-github />
</a>

<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---
layout: image-right
image: https://source.unsplash.com/collection/94734566/1920x1080
---

# React JS

React is a JavaScript library for building UI
components for web applications. React was
developed by Facebook and was released in 2013.
Facebook extensively uses React in their products
like Facebook, WhatsApp, Instagram etc. It is used
by Facebook, Uber, Netflix, Twitter, Reddit, Paypal,
Walmart, and many other popular websites.

---

# React JS

- is declarative
- is simple
- is component based
- supports server side
- is easy to learn
- use JSX for templating

---

# React JS

- A package manager, such as **Yarn** or **npm**. It lets you take advantage of a vast ecosystem of third-party packages, and easily install or update them.
- A bundler, such as **webpack**. It lets you write modular code and bundle it together into small packages to optimize load time.
- A compiler such as **Babel**. It lets you write modern JavaScript code that still works in older browsers.

---

# Installation - Manual

- Buat folder project baru, kemudian init package manager

```bash
$ mkdir learn-react
$ cd learn-react
$ npm init
```

- Install `webpack`

```bash
$ npm install webpack webpack-cli webpack-dev-server html-webpack-plugin -D
```

- Konfigurasi `webpack` dalam file `webpack.config.js`
- Install `babel`

```bash
$ npm install @babel/core @babel/preset-env @babel/preset-react babel-loader -D
```

- Install React

```bash
$ npm install --save react react-dom
```

---

# Installation - create-react-app

- Install menggunakan package manager

```bash
$ npx create-react-app learn-react
```

- Buka folder yang sudah ter-install

```bash
$ cd learn-react
```

- Jalankan

```bash
$ npm start
```

---

# JSX

<div grid="~ cols-2 gap-4">
<div>

JSX, which stands for JavaScript XML.
With JSX, we can write what looks like
HTML, and also we can create and use
our own XML-like tags. Using JSX is not
mandatory for writing React. Under the
hood, it's running `createElement()`. JSX is
easier to write and understand than
creating and appending many elements
in vanilla JavaScript, and is one of the
reasons people love React so much.

</div>
<div>

```js {all|1-2|4-9|all}
// using jsx
<h1 className="title">This is using JSX</h1>;

// not using JSX
React.createElement("h1", { className: "title" }, "This is not using JSX");
```

</div>
</div>

---

# JSX

JSX is actually closer to JavaScript, not HTML, so there are a few key
differences to note when writing it.

- `className` is used instead of `class` for adding CSS classes, as `class` is a
  reserved keyword in JavaScript.
- Properties and methods in JSX are camelCase - `onclick` will become
  `onClick`.
- Self-closing tags must end in a slash - e.g. `<Image />`

---
layout: center
class: text-center
---

# Learn More

[Documentations](https://sli.dev) / [GitHub Repo](https://github.com/slidevjs/slidev)
