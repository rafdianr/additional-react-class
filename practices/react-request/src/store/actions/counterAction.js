export const addCounter = () => {
  return {
    type: "ADD_COUNTER"
  }
}

export const subCounter = () => {
  return {
    type: "SUB_COUNTER"
  }
}