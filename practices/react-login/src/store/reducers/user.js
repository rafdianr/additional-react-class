const initialState = {
  token: '',
  email: '',
  id: ''
}

const user = (state = initialState, action) => {
  switch (action.type) {
    case "GET_USER":
      return {
        ...state,
        token: action.payload.token,
          email: action.payload.email,
          id: action.payload.id,
      }
      case "LOGOUT_USER":
        return initialState
      default:
        return state

  }
}

export default user