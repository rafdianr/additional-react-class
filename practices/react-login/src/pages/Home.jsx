import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

const Home = () => {
  const user = useSelector((state) => state.user.email);
  const token = useSelector((state) => state.user.token);

  if (!token) {
    return <Redirect to="/login" />;
  }

  return (
    <div>
      <h1>Homepage</h1>
      <h4>Hello, {user}</h4>
    </div>
  );
};

export default Home;
