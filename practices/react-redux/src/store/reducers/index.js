import {
  combineReducers
} from "redux"
import counterRed from './counterRed'
import user from './user'

export default combineReducers({
  counterRed,
  user
})