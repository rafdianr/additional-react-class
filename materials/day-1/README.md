# Day 1 - Getting Started

## Instalasi- Manual

- Buat folder project baru, kemudian init package manager

```bash
$ mkdir learn-react
$ cd learn-react
$ npm init
```

- Install `webpack`

```bash
$ npm install webpack webpack-cli webpack-dev-server html-webpack-plugin -D
```

- Konfigurasi `webpack` dalam file `webpack.config.js`

```js
const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
module.exports = {
  entry: "./src/index.js",

  output: {
    filename: "main.js",
    path: path.resolve(__dirname, "dist"),
  },

  plugins: [
    new HtmlWebPackPlugin({
      template: path.resolve(__dirname, "public/index.html"),
      filename: "index.html",
    }),
  ],

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
          },
        },
        resolve: {
          extensions: [".js", ".jsx"],
        },
      },
    ],
  },
};
```

- Install `babel`

```bash
$ npm install @babel/core @babel/preset-env @babel/preset-react babel-loader -D
```

- Install React

```bash
$ npm install --save react react-dom
```

## Instalasi - create-react-app

- Install menggunakan package manager

```bash
$ npx create-react-app learn-react
```

- Buka folder yang sudah ter-install

```bash
$ cd learn-react
```

- Jalankan

```bash
$ npm start
```

## Sumber belajar

- [Dokumentasi ReactJS](https://reactjs.org/docs/getting-started.html)
- [Instalasi manual](https://medium.com/@trihargianto/belajar-react-js-bagian-1-manual-set-up-environment-react-dengan-babel-webpack-19fd768c0408)
