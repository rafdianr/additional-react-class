---
# try also 'default' to start simple
theme: default
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://source.unsplash.com/collection/94734566/1920x1080
# apply any windi css classes to the current slide
layout: "cover"
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# some information about the slides, markdown enabled
info: |
  ## Components, State & Props
  Presentation slides for react additional class.

---

# Components, State & Props

React JS basic

<div class="pt-12">
  <span @click="$slidev.nav.next" class="px-2 p-1 rounded cursor-pointer" hover="bg-white bg-opacity-10">
    Press Space for next page <carbon:arrow-right class="inline"/>
  </span>
</div>

<a href="https://github.com/slidevjs/slidev" target="_blank" alt="GitHub"
  class="abs-br m-6 text-xl icon-btn opacity-50 !border-none !hover:text-white">
<carbon-logo-github />
</a>

<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---
layout: image-right
image: https://source.unsplash.com/collection/94734566/1920x1080
---

# Components

- Komponen adalah bagian-bagian yang menyusun aplikasi React
- Komponen di React bersifat reusable, artinya bisa digunakan kembali
- Ada 2 macam komponen yang sering digunakan, yaitu class
component dan function component
- Class component juga biasa disebut stateful component
- Function component juga biasa disebut stateless component

---

# Class Components

<div grid="~ cols-2 gap-4">
<div>

- Disebut juga Stateful Component
- Dapat menggunakan React Lifecycle
- Dapat menggunakan state & props
- Fetching data dari API/Server biasanya dilakukan disini.

</div>
<div>

```js {all|1|3-12|4-11|14|all}
import React from 'react'

class Latihan extends React.Component {
  render() {
    return (
      <div className="App">
        <h1>Latihan React!</h1>
        <h2>Menggunakan Class component</h2>
      </div>
    )
  }
}

export default Latihan
```
</div>
</div>

---

# Function Components

<div grid="~ cols-2 gap-4">
<div>

- Disebut juga Stateless Component
- Tidak dapat menggunakan React Lifecycle
- Hanya dapat menggunakan props

</div>
<div>

```js {all|1|3-10|4-9|12|all}
import React from "react";

const Latihan = (props) => {
  return (
      <div className="App">
        <h1>Latihan React!</h1>
        <h2>Menggunakan function component</h2>
      </div>
    )
}

export default Latihan
```
</div>
</div>

---

# State - Props

- State dan props adalah objek khusus yang menyimpan data untuk
komponen.
- State adalah objek yang digunakan untuk menyimpan data di dalam
komponen.
- Props adalah objek yang digunakan untuk menyimpan data yang
diterima dari komponen lainnya.
- Data yang disimpan di dalam state bisa di ubah-ubah.
- Data yang disimpan di dalam props tidak bisa diubah.

---

# Membuat state

```js
import React from 'react'

class Latihan extends React.Component {
  constructor() {
    super();
    this.state = {
      title: "State buat judul",
      subtitle: "State buat subtitle"
    }
  }
  render() {
    return (
      <div className="App">
        <h1>{this.state.title}</h1>
        <h2>{this.state.subtitle}</h2>
      </div>
    )
  }
}

export default Latihan
```
---

# Mengubah state

<div grid="~ cols-2 gap-4">
<div>

- Mengubah state menggunakan this.setState().
- Setiap kali state berubah, React akan melakukan update tampilan
komponen pada bagian yang berubah saja.



```js
import React from 'react'

class Latihan extends React.Component {
  constructor() {
    super();
    this.state = {
      title: "State buat judul",
      description: "State buat description"
    }
  }

...
```
</div>
<div>

```js
...

  changeTitle = () => {
    this.setState({
      title: "Judul berubah"
    })
  }

  render() {
    return (
      <div className="App">
        <h1>{this.state.title}</h1>
        <p>{this.state.description}</p>
        <button onClick={this.changeTitle}>Ubah judul</button>
      </div>
    )
  }
}

export default Latihan
```
</div>
</div>

---

# Mengelola props

```js
import React from 'react'

const User = (props) => {
  return (
    <div>
      <h4>{props.name}</h4>
      <p>{props.email}</p>
    </div>
  )
}
```

---
layout: center
class: text-center
---

# Learn More

[Documentations](https://sli.dev) / [GitHub Repo](https://github.com/slidevjs/slidev)
