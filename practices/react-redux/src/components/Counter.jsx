import React, { Component } from "react";
import { connect } from "react-redux";

class Counter extends Component {
  render() {
    return (
      <div>
        <h1>{this.props.title}</h1>
        <h1>{this.props.counter}</h1>
        <div>
          <button onClick={this.props.increement}>+</button>
          <button onClick={this.props.decreement}>-</button>
        </div>
        <p>Barusan {this.props.status}</p>
      </div>
    );
  }
}

const stateToProps = (state) => {
  return {
    counter: state.counterRed.counter,
    title: state.counterRed.judul,
    status: state.counterRed.status,
  };
};

const dispatchToProps = (dispatch) => {
  return {
    increement: () => dispatch({ type: "ADD_COUNTER", payload: "Ditambah" }),
    decreement: () => dispatch({ type: "SUB_COUNTER", payload: "Dikurang" }),
  };
};

export default connect(stateToProps, dispatchToProps)(Counter);
