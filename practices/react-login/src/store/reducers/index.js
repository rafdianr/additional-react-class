import {
  combineReducers
} from "redux"
import counterRed from './counterRed'
import albums from './albums'
import user from './user'

export default combineReducers({
  counterRed,
  albums,
  user
})