import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "../assets/styles/card.css";
import { getAlbums } from "../store/actions/albumsAction";
import Card from "../components/Card";
import axios from "../helpers/axios";

const Hook = () => {
  const title = useSelector((state) => state.counterRed.judul);
  const dispatch = useDispatch();
  const photos = useSelector((state) => state.albums.photos);

  // fetch("url").then(response=>response.json()).then(data=>console.log(data))
  // useEffect(() => {
  //   fetch("https://jsonplaceholder.typicode.com/albums/1/photos")
  //     .then((response) => response.json())
  //     .then((data) => dispatch(getAlbums(data)))
  //     .catch((e) => console.log(e));
  // }, []);

  // axios
  useEffect(() => {
    axios
      .get("albums/1/photos")
      .then((response) => dispatch(getAlbums(response.data)))
      .catch((e) => console.log({ e }));
  }, [dispatch]);

  useEffect(() => {
    axios
      .get("albums/2/photos")
      .then((response) => console.log(response))
      .catch((e) => console.log({ e }));
  });

  return (
    <div>
      <h1>{title}</h1>
      <Card data={photos} />
    </div>
  );
};

export default Hook;
