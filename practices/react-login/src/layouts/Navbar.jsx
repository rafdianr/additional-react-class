import React from "react";
import { Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import "../assets/styles/navbar.scss";
import { logoutUser } from "../store/actions/userAction";

const Navbar = () => {
  const token = useSelector((state) => state.user.token);
  const dispatch = useDispatch();

  const onLogout = () => {
    dispatch(logoutUser());
  };
  return (
    <div className="navbar">
      <div className="navbar__item">
        <Link to="/">Home</Link>
      </div>
      {token ? (
        <Button className="navbar__item" onClick={onLogout}>
          Logout
        </Button>
      ) : (
        <div className="navbar__item">
          <Link to="/login">Login</Link>
        </div>
      )}
    </div>
  );
};
export default Navbar;
