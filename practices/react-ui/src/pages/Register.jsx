import React, { useState } from "react";
import { Form, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import axios from "../helpers/axios";
import { getUser } from "../store/actions/userAction";

const Register = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const token = useSelector((state) => state.user.token);

  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log(email, password);
    const newData = { email, password };
    setLoading(true);
    axios
      .post("register/customer", newData)
      .then((res) => {
        dispatch(getUser({ token: res.data.token, email: res.data.email }));
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        console.log({ err });
      });
  };

  if (token) {
    return <Redirect to="/" />;
  }

  return (
    <div>
      <h1>Register</h1>
      <div
        className="form-wrapper"
        style={{
          width: "50%",
          textAlign: "left",
          margin: "auto",
          maxWidth: "500px",
        }}
      >
        <Form onSubmit={handleSubmit}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>
          <Button variant="primary" type="submit" block>
            {loading ? "Loading" : "Submit"}
          </Button>
        </Form>
      </div>
    </div>
  );
};

export default Register;
