export const getServices = (payload) => {
  return {
    type: "GET_SERVICES",
    payload
  }
}