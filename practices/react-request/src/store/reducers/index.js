import {
  combineReducers
} from "redux"
import counterRed from './counterRed'
import albums from './albums'

export default combineReducers({
  counterRed,
  albums
})