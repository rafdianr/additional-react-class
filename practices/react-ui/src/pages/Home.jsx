import React, { useEffect } from "react";
import { Button, Card, CardGroup, Container } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import axios from "../helpers/axios";
import { getServices } from "../store/actions/washmeAct";

const Home = () => {
  const dispatch = useDispatch();
  const packages = useSelector((state) => state.washme.package);

  useEffect(() => {
    axios
      .get("services")
      .then((res) => dispatch(getServices(res.data.data)))
      .catch((err) => console.log({ err }));
  }, [dispatch]);

  return (
    <div>
      <Container>
        <CardGroup>
          {packages.map((item) => (
            <Card key={item.id}>
              <Card.Img variant="top" src={item.imageBasic} />
              <Card.Body>
                <Card.Title>{item.services}</Card.Title>
              </Card.Body>
            </Card>
          ))}
        </CardGroup>
      </Container>
    </div>
  );
};

export default Home;
