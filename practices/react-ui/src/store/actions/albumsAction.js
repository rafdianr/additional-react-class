export const getAlbums = (payload) => {
  return {
    type: "GET_ALBUMS",
    payload
  }
}