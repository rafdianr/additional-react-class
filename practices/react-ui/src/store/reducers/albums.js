const initialState = {
  photos: []
}

const albums = (state = initialState, action) => {
  switch (action.type) {
    case "GET_ALBUMS":
      return {
        ...state,
        photos: action.payload
      }
      default:
        return state
  }
}

export default albums