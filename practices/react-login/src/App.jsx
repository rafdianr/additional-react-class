import "./assets/styles/style.css";
import React from "react";
import { Switch, Route, BrowserRouter, Redirect } from "react-router-dom";
import Navbar from "./layouts/Navbar";
import Footer from "./layouts/Footer";
import Home from "./pages/Home";
import { Provider } from "react-redux";
import configureStore from "./store/configure-store";
import { PersistGate } from "redux-persist/integration/react";
import Login from "./pages/Login";
import Register from "./pages/Register";
import "bootstrap/dist/css/bootstrap.min.css";

const { persistor, store } = configureStore();
function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <BrowserRouter>
          <div className="screen">
            <div className="wrapper">
              <Navbar />
              <hr />
              <Switch>
                <Route path="/" exact>
                  <Home />
                </Route>
                <Route path="/login" exact>
                  <Login />
                </Route>
                <Route path="/register" exact>
                  <Register />
                </Route>
                <Route path="*" exact>
                  <Redirect to="/" />
                </Route>
              </Switch>
              <Footer content="Konten footer" />
            </div>
          </div>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
}

export default App;
