export const addCounter = (payload) => {
  return {
    type: "ADD_COUNTER",
    payload,
  };
};

export const subCounter = (payload) => {
  return {
    type: "SUB_COUNTER",
    payload,
  };
};
