# REST API

**API** adalah singkatan dari Application Programming Interface yaitu sebuah software yang memungkinkan para developer untuk mengintegrasikan dan mengizinkan dua aplikasi yang berbeda secara bersamaan untuk saling terhubung satu sama lain.

**RESTful API** / **REST API** merupakan implementasi dari API (Application Programming Interface). REST (Representional State Transfer) adalah suatu arsitektur metode komunikasi yang menggunakan protokol HTTP untuk pertukaran data dan metode ini sering diterapkan dalam pengembangan aplikasi.

- Sangat mudah dipelajari dan dipahami.
- Ini memberi pengembang kemampuan untuk mengatur aplikasi yang rumit
  menjadi sumber daya sederhana.
- REST API tidak khusus untuk bahasa atau platform, tetapi dapat digunakan dengan
  bahasa apa pun atau berjalan di platform apa pun.

## fetch()

```js
useEffect(() => {
  fetch("https://jsonplaceholder.typicode.com/albums/1/photos")
    .then((response) => response.json())
    .then((data) => dispatch(getAlbums(data)));
}, [dispatch]);
```

## axios

```js
useEffect(() => {
  axios
    .get(`albums/1/photos`)
    .then((res) => {
      dispatch(getAlbums(res.data));
    })
    .catch((error) => console.log({ error }));
}, [dispatch]);
```
