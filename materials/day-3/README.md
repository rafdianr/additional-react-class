# Lifecycle & Hooks

## Lifecycle

### Mounting

Method-method pada fase Mounting ini akan dieksekusi saat komponen
sedang dibuat dan akan dimasukkan ke dalam DOM. Dengan kata lain,
fase ini adalah fase ketika pertama kali me-render sebuah komponen.

1. constructor()
2. render()
3. componentDidMount()

### Updating

Method-method pada fase Updating ini akan dieksekusi saat ada
perubahan pada state atau props. Dengan kata lain, fase ini adalah fase
re-render sebuah komponen.

1. render()
2. componentDidUpdate()

### Unmounting

Method pada fase Unmounting ini akan dieksekusi saat komponen
di-remove/dihilangkan dari DOM.

1. componentWillUnmount()

## Hook

React Hooks membuat penulisan component lebih simple dan
elegan. Dengan React Hooks, kita dapat menggunakan state dan
fitur lainnya tanpa menggunakan class component.

1. Simplicity — Kemudahan dalam penulisan
2. State & Props — Data bagi sang component
3. Lifecycle — Siklus hidup tumbuhan React
4. Pure Component — Component murni

### useState

`useState` merupakan salah satu hook yang digunakan untuk membuat sebuah state.

```javascript
import React, { useState } from "react";

const App = () => {
  const [state, setState] = useState(initialState);

  const [number, setNumber] = useState(0);
  const [name, setName] = useState("Jordan");
  const [fruits, setFruits] = useState(["Apple", "Banana", "Orange"]);

  //...
};

export default App;
```

### useEffect

`useEffect` memiliki fungsi yang sama `componentDidMount`,
`componentDidUpdate`, and `componentWillUnmount` dalam React class component,
tetapi disajikan dalam satu API.

```js
useEffect(() => {
  effect;
  return () => {
    cleanup;
  };
}, [input]);
```
